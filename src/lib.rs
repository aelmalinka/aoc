#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![deny(unsafe_op_in_unsafe_fn)]
#![feature(let_chains)]

use snafu::Snafu;

#[derive(Debug, Snafu)]
#[snafu(visibility(pub(crate)))]
pub enum Error {
    Y2022 { source: y2022::Error },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

mod util;

pub mod y2022;
