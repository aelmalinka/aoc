#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![deny(unsafe_op_in_unsafe_fn)]

use aoc::Error;
use async_std::task;
use snafu::Report;

#[async_std::main]
async fn main() -> Report<Error> {
    simple_logger::init_with_level(log::Level::Debug).unwrap();
    println!("Year 2022");

    let res = futures::try_join!(
        //task::spawn(async move { Ok(format!("challenge 1 {}", aoc::y2022::c01::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 2 {}", aoc::y2022::c02::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 3 {}", aoc::y2022::c03::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 4 {}", aoc::y2022::c04::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 5 {}", aoc::y2022::c05::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 6 {}", aoc::y2022::c06::main().await?)) }),
        task::spawn(async move { Ok(format!("challenge 7 {}", aoc::y2022::c07::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 7 {}", aoc::y2022::c07::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 7 {}", aoc::y2022::c07::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 8 {}", aoc::y2022::c08::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge 9 {}", aoc::y2022::c09::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge10 {}", aoc::y2022::c10::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge11 {}", aoc::y2022::c11::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge12 {}", aoc::y2022::c12::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge13 {}", aoc::y2022::c13::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge14 {}", aoc::y2022::c14::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge15 {}", aoc::y2022::c15::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge16 {}", aoc::y2022::c16::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge17 {}", aoc::y2022::c17::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge18 {}", aoc::y2022::c18::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge19 {}", aoc::y2022::c19::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge20 {}", aoc::y2022::c20::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge21 {}", aoc::y2022::c21::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge22 {}", aoc::y2022::c22::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge23 {}", aoc::y2022::c23::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge24 {}", aoc::y2022::c24::main().await?)) }),
        //task::spawn(async move { Ok(format!("challenge25 {}", aoc::y2022::c25::main().await?)) }),
    )?;

    println!("{}", res.0);

    Report::ok()
}
