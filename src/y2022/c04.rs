use std::num::ParseIntError;

use super::C04Snafu;
use crate::{Result, Y2022Snafu};

use async_std::task;
use itertools::Itertools;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    NoComma,
    NotInt { source: ParseIntError },
}

#[derive(Copy, Clone, Debug)]
struct Sections(u8, u8);

impl Sections {
    #[allow(dead_code)]
    const fn contains(self, other: Self) -> bool {
        (self.0 >= other.0 && self.1 <= other.1) || (self.0 <= other.0 && self.1 >= other.1)
    }
    const fn within(self, other: Self) -> bool {
        if self.0 < other.0 {
            self.1 >= other.0
        } else {
            other.1 >= self.0
        }
    }
}

impl TryFrom<&str> for Sections {
    type Error = crate::Error;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let (a, b) = value
            .split_once('-')
            .ok_or_else(|| NoCommaSnafu.build())
            .context(C04Snafu)
            .context(Y2022Snafu)?;

        let a = a
            .parse()
            .context(NotIntSnafu)
            .context(C04Snafu)
            .context(Y2022Snafu)?;
        let b = b
            .parse()
            .context(NotIntSnafu)
            .context(C04Snafu)
            .context(Y2022Snafu)?;

        Ok(Self(a, b))
    }
}

#[derive(Copy, Clone, Debug)]
struct Pair(Sections, Sections);

impl Pair {
    #[allow(dead_code)]
    const fn contains(self) -> bool {
        self.0.contains(self.1)
    }
    const fn within(self) -> bool {
        self.0.within(self.1)
    }
}

impl TryFrom<&str> for Pair {
    type Error = crate::Error;
    fn try_from(value: &str) -> Result<Self> {
        let (a, b) = value
            .split_once(',')
            .ok_or_else(|| NoCommaSnafu.build())
            .context(C04Snafu)
            .context(Y2022Snafu)?;

        Ok(Self(a.try_into()?, b.try_into()?))
    }
}

/// Challenge 04
/// # Errors
pub async fn main() -> Result<usize> {
    task::spawn_blocking(|| {
        Ok(INPUT
            .lines()
            .map(Pair::try_from)
            .try_collect::<Pair, Vec<_>, crate::Error>()?
            .into_iter()
            //.filter(Pair::contains)
            .filter(|&i| i.within())
            .count())
    })
    .await
}

const INPUT: &str = include_str!("c04.input");
