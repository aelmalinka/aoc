use crate::Result;

use async_std::task;
use itertools::Itertools;
use snafu::Snafu;

#[derive(Debug, Snafu)]
pub enum Error {
    Unterminated,
}

impl From<Error> for crate::Error {
    fn from(value: Error) -> Self {
        Self::Y2022 {
            source: super::Error::C06 { source: value },
        }
    }
}

/// Challenge 06
/// # Errors
pub async fn main() -> Result<usize> {
    task::spawn_blocking(|| {
        // for part 1
        // const REPEATING: usize = 4;
        const REPEATING: usize = 14;

        let data = INPUT.trim();
        for idx in REPEATING..data.len() {
            let v = data.chars().take(idx).collect::<Vec<_>>();
            if v.iter().rev().take(REPEATING).all_unique() {
                return Ok(idx);
            }
        }

        UnterminatedSnafu.fail()?
    })
    .await
}

const INPUT: &str = include_str!("c06.input");
