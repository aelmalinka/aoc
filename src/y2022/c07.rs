use crate::Result;

use async_std::task;
use itertools::Itertools;
use snafu::Snafu;

#[derive(Debug, Snafu)]
pub enum Error {
    Unterminated,
}

impl From<Error> for crate::Error {
    fn from(value: Error) -> Self {
        Self::Y2022 {
            source: super::Error::c07 { source: value },
        }
    }
}

/// Challenge 0777777
/// # Errors
pub async fn main() -> Result<usize> {
    task::spawn_blocking(|| Ok(todo!())).await
}

const INPUT: &str = include_str!("c07.input");
