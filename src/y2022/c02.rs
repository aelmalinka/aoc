use super::C02Snafu;
use crate::{Result, Y2022Snafu};

use async_std::task;
use itertools::Itertools;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    CharMissing,
    InvalidState { value: String },
    InvalidRps { value: char },
}

#[derive(Clone, Copy, Debug)]
enum Rps {
    Rock,
    Paper,
    Scissors,
}

#[derive(Clone, Copy, Debug)]
enum State {
    Win,
    Draw,
    Lose,
}

#[derive(Clone, Copy, Debug)]
struct Round {
    elf: Rps,
    me: Rps,
}

impl TryFrom<&str> for Round {
    type Error = crate::Error;

    fn try_from(value: &str) -> Result<Self> {
        /* // note: for first challenge
        Self {
            elf: value.chars().nth(0).unwrap().into(),
            me: value.chars().nth(2).unwrap().into(),
        }
        */
        // note: for second challenge
        let elf = value
            .chars()
            .next()
            .ok_or_else(|| CharMissingSnafu.build())
            .context(C02Snafu)
            .context(Y2022Snafu)?
            .try_into()?;
        let me = match value
            .chars()
            .nth(2)
            .ok_or_else(|| CharMissingSnafu.build())
            .context(C02Snafu)
            .context(Y2022Snafu)?
        {
            'X' => match elf {
                Rps::Rock => Rps::Scissors,
                Rps::Paper => Rps::Rock,
                Rps::Scissors => Rps::Paper,
            },
            'Y' => elf,
            'Z' => match elf {
                Rps::Rock => Rps::Paper,
                Rps::Paper => Rps::Scissors,
                Rps::Scissors => Rps::Rock,
            },
            _ => InvalidStateSnafu { value }
                .fail()
                .context(C02Snafu)
                .context(Y2022Snafu)?,
        };
        Ok(Self { elf, me })
    }
}

impl TryFrom<char> for Rps {
    type Error = crate::Error;

    fn try_from(value: char) -> Result<Self> {
        Ok(match value {
            'A' | 'X' => Self::Rock,
            'B' | 'Y' => Self::Paper,
            'C' | 'Z' => Self::Scissors,
            _ => InvalidRpsSnafu { value }
                .fail()
                .context(C02Snafu)
                .context(Y2022Snafu)?,
        })
    }
}

impl From<Round> for State {
    fn from(value: Round) -> Self {
        match value {
            Round {
                elf: Rps::Scissors,
                me: Rps::Rock,
            }
            | Round {
                elf: Rps::Rock,
                me: Rps::Paper,
            }
            | Round {
                elf: Rps::Paper,
                me: Rps::Scissors,
            } => Self::Win,
            Round {
                elf: Rps::Rock,
                me: Rps::Rock,
            }
            | Round {
                elf: Rps::Paper,
                me: Rps::Paper,
            }
            | Round {
                elf: Rps::Scissors,
                me: Rps::Scissors,
            } => Self::Draw,
            Round {
                elf: Rps::Rock,
                me: Rps::Scissors,
            }
            | Round {
                elf: Rps::Paper,
                me: Rps::Rock,
            }
            | Round {
                elf: Rps::Scissors,
                me: Rps::Paper,
            } => Self::Lose,
        }
    }
}

impl From<Round> for u32 {
    fn from(value: Round) -> Self {
        let state: State = value.into();

        <State as Into<Self>>::into(state) + <Rps as Into<Self>>::into(value.me)
    }
}

impl From<Rps> for u32 {
    fn from(value: Rps) -> Self {
        match value {
            Rps::Rock => 1,
            Rps::Paper => 2,
            Rps::Scissors => 3,
        }
    }
}

impl From<State> for u32 {
    fn from(value: State) -> Self {
        match value {
            State::Win => 6,
            State::Draw => 3,
            State::Lose => 0,
        }
    }
}

/// Challenge 02
/// # Errors
/// - parse failure
pub async fn main() -> Result<u32> {
    task::spawn_blocking(|| {
        Ok(INPUT
            .lines()
            .map(Round::try_from)
            .try_collect::<Round, Vec<Round>, crate::Error>()?
            .into_iter()
            .map(<Round as Into<u32>>::into)
            .sum())
    })
    .await
}

const INPUT: &str = include_str!("c02.input");
