use std::num::ParseIntError;

use super::C05Snafu;
use crate::{Result, Y2022Snafu};

use async_std::task;
use itertools::Itertools;
use snafu::{ensure, ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    InvalidInput { value: String },
    InvalidStack { value: String },
    InvalidCrate { value: Vec<char> },
    InvalidStackDef { source: ParseIntError },
    InvalidOperation { value: String },
    InvalidOperationPart { source: ParseIntError },
}

#[derive(Clone, Debug, Copy)]
struct Crate(char);

#[derive(Clone, Debug)]
struct Stack(Vec<Crate>);

#[derive(Clone, Debug)]
struct Deck(Vec<Stack>);

#[derive(Clone, Copy, Debug)]
struct Operation {
    start: usize,
    end: usize,
    count: usize,
}

impl From<Error> for crate::Error {
    fn from(value: Error) -> Self {
        Self::Y2022 {
            source: super::Error::C05 { source: value },
        }
    }
}

impl TryFrom<&str> for Operation {
    type Error = crate::Error;
    fn try_from(value: &str) -> Result<Self> {
        let [mv, count, from, start, to, end] = value.split(' ').collect::<Vec<_>>()[..] else {
            return InvalidOperationSnafu { value }.fail()?;
        };

        ensure!(mv == "move", InvalidOperationSnafu { value });
        ensure!(from == "from", InvalidOperationSnafu { value });
        ensure!(to == "to", InvalidOperationSnafu { value });

        let count = count.parse::<usize>().context(InvalidOperationPartSnafu)?;
        let start = start.parse::<usize>().context(InvalidOperationPartSnafu)? - 1;
        let end = end
            .trim()
            .parse::<usize>()
            .context(InvalidOperationPartSnafu)?
            - 1;

        Ok(Self { start, end, count })
    }
}

impl Deck {
    fn apply(&mut self, operation: Operation) {
        // for part 1
        /*let idx = operation.count;
        for _ in 0..idx {
            let item = self.0[operation.start].0.pop().unwrap();
            log::debug!("moving {:?}", item);
            self.0[operation.end].0.push(item);
        }*/
        // for part 2
        let size = self.0[operation.start].0.len();
        let mut moving = self.0[operation.start].0.split_off(size - operation.count);
        self.0[operation.end].0.append(&mut moving);
    }
    fn top(&self) -> impl Iterator<Item = char> + '_ {
        self.0.iter().filter_map(|i| i.0.last()).map(|i| i.0)
    }
}

impl TryFrom<&str> for Deck {
    type Error = crate::Error;
    fn try_from(value: &str) -> Result<Self> {
        let (crates, count) = value
            .rsplit_once("\n ")
            .ok_or_else(|| InvalidStackSnafu { value }.build())?;

        let count = count
            .trim()
            .split(' ')
            .filter_map(|i| match i.trim() {
                "" => None,
                s => Some(s.parse()),
            })
            .try_collect::<usize, Vec<_>, ParseIntError>()
            .context(InvalidStackDefSnafu)?
            .into_iter()
            .max()
            .ok_or_else(|| InvalidStackSnafu { value }.build())?;
        let crates = crates.chars().chunks(4);

        let mut i = 0;
        Ok(Self(
            crates
                .into_iter()
                .map(|i| {
                    let s = i.collect::<Vec<_>>();
                    match s.as_slice() {
                        ['[', c, ']'] if c != &' ' => Ok(Some(Crate(*c))),
                        ['[', c, ']', ' ' | '\n'] if c != &' ' => Ok(Some(Crate(*c))),
                        [' ', ' ', ' ', ' ' | '\n'] => Ok(None),
                        _ => InvalidCrateSnafu { value: s }
                            .fail()
                            .context(C05Snafu)
                            .context(Y2022Snafu),
                    }
                })
                .try_collect::<Option<Crate>, Vec<_>, crate::Error>()?
                .into_iter()
                .fold(vec![], |mut acc, next| {
                    if acc.len() <= i {
                        acc.push(vec![]);
                    }

                    if let Some(c) = next {
                        acc[i].insert(0, c);
                    }

                    i = (i + 1) % count;

                    acc
                })
                .into_iter()
                .map(Stack)
                .collect::<Vec<_>>(),
        ))
    }
}

/// Challenge 05
/// # Errors
pub async fn main() -> Result<String> {
    task::spawn_blocking(|| {
        let (deck, operations) = INPUT
            .split_once("\n\n")
            .ok_or_else(|| InvalidInputSnafu { value: INPUT }.build())
            .map_err(|e| {
                log::error!("{:?}", e);
                e
            })?;

        let mut deck = Deck::try_from(deck).map_err(|e| {
            log::error!("{:?}", e);
            e
        })?;
        operations
            .lines()
            .map(Operation::try_from)
            .try_collect::<Operation, Vec<_>, crate::Error>()?
            .into_iter()
            .for_each(|o| deck.apply(o));

        Ok(deck.top().collect::<String>())
    })
    .await
}

const INPUT: &str = include_str!("c05.input");
