use super::ParseC01Snafu;
use crate::{Result, Y2022Snafu};
use async_std::task;
use itertools::Itertools;
use snafu::ResultExt;

/// Challenge 01
/// # Errors
/// - Parse failure
pub async fn main() -> Result<i32> {
    task::spawn_blocking(|| {
        Ok(INPUT
            .lines()
            .fold(vec![vec![]], |mut acc, next| {
                let len = acc.len();

                if next.is_empty() {
                    acc.push(Vec::new());
                } else {
                    acc[len - 1].push(next.to_owned());
                }

                acc
            })
            .into_iter()
            .map(|i| {
                i.into_iter()
                    .map(|s| {
                        s.trim()
                            .parse::<i32>()
                            .context(ParseC01Snafu { s })
                            .context(Y2022Snafu)
                    })
                    .try_collect()
            })
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .map(|i: Vec<i32>| i.into_iter().sum::<i32>())
            .sorted()
            .rev()
            .take(3)
            .sum::<i32>())
    })
    .await
}

const INPUT: &str = include_str!("c01.input");
