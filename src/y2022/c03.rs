use super::C03Snafu;
use crate::{Result, Y2022Snafu};

use async_std::task;
use itertools::Itertools;
use snafu::{ResultExt, Snafu};

#[derive(Debug, Snafu)]
pub enum Error {
    InvalidLetter {
        value: char,
    },
    NoBadge {
        first: RuckSack,
        second: RuckSack,
        third: RuckSack,
    },
}

#[derive(Clone, Debug)]
pub struct RuckSack {
    first: String,
    second: String,
}

impl RuckSack {
    // use for first challenge only
    #[allow(dead_code)]
    fn shared(&self) -> char {
        self.first
            .chars()
            .find(|&i| self.second.contains(i))
            .unwrap()
    }
    fn contents(&self) -> String {
        self.first.chars().chain(self.second.chars()).collect()
    }
}

impl From<&str> for RuckSack {
    fn from(value: &str) -> Self {
        let (first, second) = value.split_at(value.len() / 2);

        Self {
            first: first.to_owned(),
            second: second.to_owned(),
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Priority(u32);

impl From<Priority> for u32 {
    fn from(value: Priority) -> Self {
        value.0
    }
}

impl TryFrom<char> for Priority {
    type Error = crate::Error;
    fn try_from(value: char) -> Result<Self> {
        if value.is_ascii_uppercase() {
            Ok(Self(value as u32 - 'A' as u32 + 27))
        } else if value.is_ascii_lowercase() {
            Ok(Self(value as u32 - 'a' as u32 + 1))
        } else {
            InvalidLetterSnafu { value }
                .fail()
                .context(C03Snafu)
                .context(Y2022Snafu)
        }
    }
}

/// Challenge 03
/// # Errors
pub async fn main() -> Result<u32> {
    task::spawn_blocking(|| {
        Ok(INPUT
            .lines()
            .map(RuckSack::from)
            // part one
            //.map(|i| i.shared())
            .batching(|i| {
                i.next()
                    .and_then(|x| i.next().and_then(|y| i.next().map(|z| (x, y, z))))
            })
            .map(|(first, second, third)| {
                first
                    .contents()
                    .chars()
                    .filter(|i| second.contents().chars().contains(i))
                    .find(|i| third.contents().chars().contains(i))
                    .ok_or_else(|| {
                        NoBadgeSnafu {
                            first,
                            second,
                            third,
                        }
                        .build()
                    })
                    .context(C03Snafu)
                    .context(Y2022Snafu)
            })
            .try_collect::<char, Vec<_>, crate::Error>()?
            .into_iter()
            .map(Priority::try_from)
            .try_collect::<Priority, Vec<_>, crate::Error>()?
            .into_iter()
            .map(u32::from)
            .sum::<u32>())
    })
    .await
}

const INPUT: &str = include_str!("c03.input");
