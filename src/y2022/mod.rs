pub mod c01;
pub mod c02;
pub mod c03;
pub mod c04;
pub mod c05;
pub mod c06;
pub mod c07;

use std::num::ParseIntError;

use snafu::Snafu;

#[derive(Debug, Snafu)]
pub enum Error {
    ParseC01 { s: String, source: ParseIntError },
    C02 { source: c02::Error },
    C03 { source: c03::Error },
    C04 { source: c04::Error },
    C05 { source: c05::Error },
    C06 { source: c06::Error },
    c07 { source: c07::Error },
}
