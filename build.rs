const BRIDGES: &[&str] = &["src/util/mod.rs"];
const FILES: &[&str] = &[];

fn main() {
    cxx_build::bridges(BRIDGES)
        .files(FILES)
        .warnings(true)
        .warnings_into_errors(true)
        .flag_if_supported("-pedantic")
        .flag_if_supported("-std=c++20")
        .flag_if_supported("/std:c++20")
        .flag_if_supported("/EHsc")
        .compile("aoc-cxx")
}
